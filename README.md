# integracao-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


# Curso

## Componente com TypeScript

**Arquivo:** *src/App.vue*

```vue
<template>
  <div id="app">
    <Contador :valorInicial="10"/>
  </div>
</template>

<script lang="ts">
import { Component, Vue } from 'vue-property-decorator'
import Contador from '@/componentes/Contador.vue'

@Component({
  components: {
    Contador
  },
})
export default class App extends Vue {}
</script>
```

**Arquivo:** *src/componentes/Contador.vue*

```vue
<template>
  <div>
    <h1>Contador</h1>
    <h2>{{ valor }}</h2>
    <button @click="setValor(-1)">Dec</button>
    <button @click="setValor(1)">Enc</button>
  </div>
</template>

<script lang="ts">
import Vue from 'vue'

export default Vue.extend({
  props: {
    valorInicial: {
      type: Number
    }
  },
  data() {
    return {
      valor: this.valorInicial || 0
    }
  },
  methods: {
    setValor(delta: number) {
      this.valor += delta
    }
  }
})
</script>
```


## Componente de Classe \#01

**Arquivo:** *src/componentes/ContadorClasse.vue*

```vue
<template>
  <div>
    <h1>Contador (Classe)</h1>
    <h2>{{ valor }}</h2>
    <button @click="setValor(-1)">Dec</button>
    <button @click="setValor(1)">Inc</button>
  </div>
</template>

<script lang="ts">
import { Vue, Component } from 'vue-property-decorator'

@Component({})
export default class ContadorClasse extends Vue {
  private valor: number = 0

  public setValor(delta: number) {
    this.valor += delta
  }
}
</script>
```

## Componente de Classe \#02
### Props

**Arquivo:** *src/App.vue*

```vue
<template>
  <div id="app">
    <Contador :valorInicial="10"/>
    <ContadorClasse :valorInicial="100"/>
  </div>
</template>

<script lang="ts">
import { Component, Vue } from 'vue-property-decorator'
import Contador from '@/componentes/Contador.vue'
import ContadorClasse from '@/componentes/ContadorClasse.vue'

@Component({
  components: {
    Contador,
    ContadorClasse
  },
})
export default class App extends Vue {}
</script>
```

**Arquivo:** *src/componentes/ContadorClasse.vue*

```vue
<template>
  <div>
    <h1>Contador (Classe)</h1>
    <h2>{{ valor }}</h2>
    <button @click="setValor(-1)">Dec</button>
    <button @click="setValor(1)">Inc</button>
  </div>
</template>

<script lang="ts">
import { Vue, Component, Prop } from 'vue-property-decorator'

@Component({})
export default class ContadorClasse extends Vue {
  @Prop(Number) private readonly valorInicial?: number

  // Ponto de exclamação (!:) é um campo obrigatório
  // @Prop(Number) private readonly valorInicial!: number

  private valor: number = this.valorInicial || 0

  public setValor(delta: number) {
    this.valor += delta
  }
}
</script>
```


## Componente de Classe \#03
### Methods e Computed

O professor deu o exemplo do Decotador `@noCache` em:
https://github.com/vuejs/vue-class-component#caveats-of-class-properties

**Arquivo:** *src/componentes/ContadorClasse.vue*

```vue
<template>
  <div>
    <h1>Contador (Classe)</h1>
    <h2>{{ valor }}</h2>
    <h3>{{ parOuImpar }}</h3>
    <button @click="setValor(-1)">Dec</button>
    <button @click="setValor(1)">Inc</button>
  </div>
</template>

<script lang="ts">
import { Vue, Component, Prop } from 'vue-property-decorator'

@Component({})
export default class ContadorClasse extends Vue {
  @Prop(Number) private readonly valorInicial?: number
  private valor: number = this.valorInicial || 0

  public setValor(delta: number) {
    this.valor += delta
  }

  // Methods / Método
  public getParOuImpar(): string {
    return this.valor % 2 === 0 ? 'Par' : 'Ímpar'
  }

  // Computed / Computado
  public get parOuImpar(): string {
    return this.valor % 2 === 0 ? 'Par' : 'Ímpar'
  }
}
</script>
```
```vue
```
```vue
```
```vue
```
